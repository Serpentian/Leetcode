/* First Unique Character in a String
 *
 * Given a string s, find the first non-repeating character in it
 * and return its index. If it does not exist, return -1.
 */

#include <algorithm>
#include <string>
#include <utility>

class Solution {
public:
  int firstUniqChar(std::string s) {
    // the number of chars encountered, min pos
    std::array<std::pair<size_t, size_t>, 26> arr;
    for (size_t i = 0; i < s.size(); ++i) {
      auto &value = arr[s[i] - 'a'];
      if (value.first) {
        ++value.first;
      } else {
        value = {1, i};
      }
    }

    int value(-1);
    std::for_each(arr.begin(), arr.end(), [&value](const auto &p1) {
      if (p1.first == 1 && (value == -1 || p1.second < value)) {
        value = p1.second;
      }
    });

    return value;
  }
};
