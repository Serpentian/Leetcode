/* Valid Sudoku
 *
 * Determine if a 9 x 9 Sudoku board is valid. Only the filled cells
 * need to be validated according to the following rules:
 *
 *   - Each row must contain the digits 1-9 without repetition.
 *   - Each column must contain the digits 1-9 without repetition.
 *   - Each of the nine 3 x 3 sub-boxes of the grid must contain the digits
 *     1-9 without repetition.
 *
 * Note:
 *
 *   A Sudoku board (partially filled) could be valid but is not
 *   necessarily solvable.
 *
 *   Only the filled cells need to be validated according to the
 *   mentioned rules.
 */

#include <cassert>
#include <cstdlib>
#include <vector>

#define BOARD_ROWS 9
#define BOARD_COLUMNS 9
#define BOARD_BOXES 9
#define VARIABLES_NUM 9

class Solution {
public:
  bool isValidSudoku(std::vector<std::vector<char>> &board) {
    bool row_checker[BOARD_ROWS][VARIABLES_NUM] = {false},
         column_checker[BOARD_COLUMNS][VARIABLES_NUM] = {false},
         boxes_checker[BOARD_BOXES][VARIABLES_NUM] = {false};

    for (size_t i = 0; i < board.size(); ++i) {
      for (size_t j = 0; j < board[i].size(); ++j) {
        if (board[i][j] != '.') {
          size_t ival = board[i][j] - '0' - 1,
                 box_num = i / 3 * 3 + j / 3;
          if (row_checker[i][ival] || column_checker[j][ival] ||
              boxes_checker[box_num][ival]) {
            return false;
          }

          row_checker[i][ival] = column_checker[j][ival] =
              boxes_checker[box_num][ival] = true;
        }
      }
    }

    return true;
  }
};
