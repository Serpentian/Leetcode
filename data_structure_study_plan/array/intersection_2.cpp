/* Intersection of Two arrays
 *
 * Given two integer arrays nums1 and nums2, return an array
 * of their intersection. Each element in the result must
 * appear as many times as it shows in both arrays and you may
 * return the result in any order.
 */

#include <algorithm>
#include <cstdlib>
#include <unordered_map>
#include <vector>

class Solution {
public:
  // time is more important than space usage
  std::vector<int> intersect1(std::vector<int> &nums1,
                              std::vector<int> &nums2) {
    std::vector<int> to_hash_map, to_iterate;
    if (nums1.size() > nums2.size()) {
      to_hash_map = nums1;
      to_iterate = nums2;

    } else {
      to_hash_map = nums2;
      to_iterate = nums1;
    }

    std::unordered_map<int, size_t> map;
    map.reserve(to_hash_map.size());

    for (const auto &x : to_hash_map) {
      auto iter = map.find(x);
      if (iter != map.end()) {
        ++iter->second;
      } else {
        map.insert({x, 1});
      }
    }

    std::vector<int> result;
    for (const auto &x : to_iterate) {
      auto iter = map.find(x);
      if (iter != map.end()) {
        result.push_back(iter->first);
        --iter->second;
        if (!iter->second) {
          map.erase(iter);
        }
      }
    }

    return result;
  }

  // vice versa
  std::vector<int> intersect2(std::vector<int> &nums1,
                              std::vector<int> &nums2) {
    std::sort(nums1.begin(), nums1.end());
    std::sort(nums2.begin(), nums2.end());

    std::vector<int> result;
    auto iter1 = nums1.begin(), iter2 = nums2.begin();
    while (iter1 != nums1.end() && iter2 != nums2.end()) {
      if (*iter1 == *iter2) {
        result.push_back(*iter1++);
        ++iter2;
      } else if (*iter1 < *iter2) {
        ++iter1;
      } else {
        ++iter2;
      }
    }

    return result;
  }
};
