/* Two Sum
 *
 * Given an array of integers nums and an integer target, return
 * indices of the two numbers such that they add up to target.
 *
 * You may assume that each input would have exactly one solution,
 * and you may not use the same element twice.
 *
 * You can return the answer in any order.
 */

#include <map>
#include <vector>

class Solution {
public:
  static std::vector<int> twoSum(std::vector<int> &nums, int target) {
    std::map<int, int> value_idx;

    for (int i = 0; i < nums.size(); ++i) {
      std::map<int, int>::iterator pos = value_idx.find(target - nums[i]);
      if (pos != value_idx.end()) {
        return std::vector<int>{i, pos->second};
      }

      value_idx.insert({nums[i], i});
    }

    return std::vector<int>();
  }
};
