/* Permutations in String
 *
 * Given two strings s1 and s2, return true if s2 contains a permutation of s1,
 * or false otherwise.
 * In other words, return true if one of s1's permutations is the substring
 * of s2.
 *
 * The solution must not be brute force
 */

/*
 * The solution can be optimised via changing frequencies
 * according to the shift
 *
 */

#include <array>
#include <string>
#include <unordered_map>
#include <iostream>

// Time Limit Exceed
class Solution {
  template <class It>
  std::unordered_map<char, size_t> getFrequencies(It first, It last) {
    std::unordered_map<char, size_t> frequencies;
    for (auto iter = first; iter < last; ++iter) {
      auto pair = frequencies.insert({*iter, 1});
      if (!pair.second) {
        ++pair.first->second;
      }
    }

    return frequencies;
  }

public:
  bool checkInclusion(std::string s1, std::string s2) {
    if (s2.size() < s1.size()) {
      return false;
    }

    auto frequencies = getFrequencies(s1.begin(), s1.end());

    // the window
    auto begin = s2.begin(), end = s2.begin() + s1.size();
    while (end <= s2.end()) {
      auto temp_freq = getFrequencies(begin, end);
      if (frequencies == temp_freq) {
        return true;
      }

      ++end, ++begin;
    }

    return false;
  }
};

class Solution2 {
  template <class It>
  std::array<size_t, 26> getFrequencies(It first, It last) {
    std::array<size_t, 26> frequencies = {};
    for (auto iter = first; iter < last; ++iter) {
      ++frequencies[*iter % 97];
    }

    return frequencies;
  }

public:
  bool checkInclusion(std::string s1, std::string s2) {
    if (s2.size() < s1.size()) {
      return false;
    }

    auto frequencies = getFrequencies(s1.begin(), s1.end());

    // the window
    auto begin = s2.begin(), end = s2.begin() + s1.size();
    while (end <= s2.end()) {
      auto temp_freq = getFrequencies(begin, end);
      if (frequencies == temp_freq) {
        return true;
      }

      ++end, ++begin;
    }

    return false;
  }
};
