/* Reverse words in a string III
 *
 * Given a string s, reverse the order of characters in each word
 * within a sentence while still preserving whitespace and initial
 * word order.
 *
 * Constraints:
 *  - s does not contain any leading or trailing spaces.
 *  - There is at least one word in s.
 *  - All the words in s are separated by a single space.
 */

#include <cstdlib>
#include <iostream>
#include <string>

class Solution {
  template <class It>
  // swaps [iter1, iter2]
  void reverString(It iter1, It iter2) {
    for (; iter1 < iter2; ++iter1, --iter2) {
      std::swap(*iter1, *iter2);
    }
  }

public:
  std::string reverseWords(std::string s) {
    // sentence does not start with " ", according to task
    auto prev_word_start = s.begin();
    for (auto iter = ++s.begin(); iter != s.end(); ++iter) {
      if (*iter == ' ') {
        reverString(prev_word_start, iter - 1);
        prev_word_start = iter + 1;
      }
    }

    reverString(prev_word_start, s.end() - 1);
    return s;
  }
};

int main() {
  std::string str;
  std::getline(std::cin, str);
  str = Solution().reverseWords(str);
  std::cout << str;
  return 0;
}
