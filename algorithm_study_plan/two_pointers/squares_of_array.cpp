/* Squares of the Sorted Array
 *
 * Given an integer array nums sorted in non-decreasing order,
 * return an array of the squares of each number sorted in
 * non-decreasing order.
 */

#include <cmath>
#include <iostream>
#include <vector>

class Solution {
public:
  std::vector<int> sortedSquares(std::vector<int> &nums) {
    auto begin = nums.begin();
    auto end = --nums.end();

    std::vector<int> answer(nums.size());
    for (size_t i = nums.size() - 1; i >= 0 && end >= begin; --i) {
      if (std::abs(*end) < std::abs(*begin)) {
        answer[i] = std::pow(*begin, 2);
        ++begin;
      } else {
        answer[i] = std::pow(*end, 2);
        --end;
      }
    }

    return answer;
  }
};

int main() {
  std::vector<int> vec = {-4, 1, 5};
  vec = Solution().sortedSquares(vec);
  for (auto x : vec) {
    std::cout << x << ' ';
  }
}
