/* Move Zeros
 *
 * Given an integer array nums, move all 0's to the end of it while
 * maintaining the relative order of the non-zero elements.
 *
 * Note that you must do this in-place without making a copy of the array.
 */

#include <cstdlib>
#include <iostream>
#include <vector>

class Solution {
public:
  void moveZeroes(std::vector<int> &nums) {
    size_t current_idx = 0;
    for (const auto &x : nums) {
      if (x) {
        nums[current_idx++] = x;
      }
    }

    for (; current_idx < nums.size(); ++current_idx) {
      nums[current_idx] = 0;
    }
  }
};
