/* Remove Nth Node from End of the List
 *
 * Given the head of a linked list, remove the nth node from the end
 * of the list and return its head.
 *
 * Constraints:
    The number of nodes in the list is sz.
    1 <= sz <= 30
    0 <= Node.val <= 100
    1 <= n <= sz
 */

#include <iostream>

struct ListNode {
  int val;
  ListNode *next;
  ListNode() : val(0), next(nullptr) {}
  ListNode(int x) : val(x), next(nullptr) {}
  ListNode(int x, ListNode *next) : val(x), next(next) {}
};

// WTF
class Solution {
public:
  ListNode *removeNthFromEnd(ListNode *head, int n) {
    ListNode *fast = head, *slow = head, *prev_slow = head;
    size_t size(0);

    while (fast) {
      for (int i = 0; i < n && fast; ++i, ++size) {
        fast = fast->next;
      }

      // slow next check is for n == 1
      if (slow->next) {
        prev_slow = slow;
        slow = slow->next;
      }
    }

    if (n == size) {
      auto tmp = head->next;
      delete head;
      return tmp;
    }

    // the size of the list is 1
    if (slow == prev_slow) {
      delete head;
      return nullptr;
    }

    prev_slow->next = slow->next;
    delete slow;
    return head;
  }
};

int main() {
  size_t size(0);
  std::cin >> size;

  int val(0);
  std::cin >> val;
  ListNode *head = new ListNode(val);
  ListNode *temp_node = head;

  for (size_t i = 1; i < size; ++i) {
    int tmp_val(0);
    std::cin >> tmp_val;

    temp_node->next = new ListNode(tmp_val);
    temp_node = temp_node->next;
  }

  int n(0);
  std::cin >> n;

  head = Solution().removeNthFromEnd(head, n);
  while (head) {
    std::cout << head->val << ' ';
    head = head->next;
  }
}
