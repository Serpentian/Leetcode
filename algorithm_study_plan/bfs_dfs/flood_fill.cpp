/* Flood Fill
 *
 * An image is represented by an m x n integer grid image where image[i][j]
 * represents the pixel value of the image.
 *
 * You are also given three integers sr, sc, and newColor. You should perform
 * a flood fill on the image starting from the pixel image[sr][sc].
 *
 * To perform a flood fill, consider the starting pixel, plus any pixels
 * connected 4-directionally to the starting pixel of the same color as
 * the starting pixel, plus any pixels connected 4-directionally to those
 * pixels (also with the same color), and so on. Replace the color of all
 * of the aforementioned pixels with newColor.
 *
 * Return the modified image after performing the flood fill.
 */

#include <queue>
#include <set>
#include <vector>


// Not the efficient way of solving task (too much memory is used)
// It's much faster to update color several times rather than
// using hash table in order to avoid collisions and queue
class Solution {
  typedef std::pair<int, int> Pixel;

public:
  // Breadth First Search
  std::vector<std::vector<int>> floodFill(std::vector<std::vector<int>> &image,
                                          int sr, int sc, int newColor) {
    // no changes needed
    if (image[sr][sc] == newColor) {
      return image;
    }

    // BFS
    int main_coor = image[sr][sc];
    std::queue<Pixel> frontier;
    // std::set<Pixel> explored;
    frontier.emplace(sr, sc);

    while (!frontier.empty()) {
      auto curr_pixel = frontier.front();
      frontier.pop();

      // if (explored.find(curr_pixel)->second ||
      //     curr_pixel.first >= image.size() ||
      //     curr_pixel.second >= image[0].size() ||
      //     image[curr_pixel.first][curr_pixel.second] != main_coor) {
      //   continue;
      // }

      if (curr_pixel.first >= image.size() ||
          curr_pixel.second >= image[0].size() ||
          image[curr_pixel.first][curr_pixel.second] != main_coor) {
        continue;
      }

      // painting
      image[curr_pixel.first][curr_pixel.second] = newColor;

      // getting neighbours
      frontier.emplace(curr_pixel.first - 1, curr_pixel.second);
      frontier.emplace(curr_pixel.first + 1, curr_pixel.second);
      frontier.emplace(curr_pixel.first, curr_pixel.second - 1);
      frontier.emplace(curr_pixel.first, curr_pixel.second + 1);

      // explored.insert(curr_pixel);
    }

    return image;
  }
};

// much more efficient one
class Solution2 {
  void bfs(std::vector<std::vector<int>> &image, int sr, int sc, int newColor,
           int oldColor) {
    if (sr >= image.size() || sc >= image[0].size() ||
        image[sr][sc] != oldColor) {
      return;
    }

    image[sr][sc] = newColor;
    bfs(image, sr - 1, sc, newColor, oldColor);
    bfs(image, sr + 1, sc, newColor, oldColor);
    bfs(image, sr, sc - 1, newColor, oldColor);
    bfs(image, sr, sc - 2, newColor, oldColor);
  }

public:
  // Breadth First Search
  std::vector<std::vector<int>> floodFill(std::vector<std::vector<int>> &image,
                                          int sr, int sc, int newColor) {
    int oldColor = image[sr][sc];
    if (oldColor != newColor) {
      bfs(image, sr, sc, newColor, oldColor);
    }

    return image;
  }
};
