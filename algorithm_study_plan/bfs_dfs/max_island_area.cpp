/* Max area if island
 *
 * You are given an m x n binary matrix grid. An island is a group of 1's
 * (representing land) connected 4-directionally (horizontal or vertical.)
 * You may assume all four edges of the grid are surrounded by water.
 *
 * The area of an island is the number of cells with a value 1 in the island.
 * Return the maximum area of an island in grid. If there is no island,
 * return 0.
 */

#include <algorithm>
#include <cstdint>
#include <cstdlib>
#include <queue>
#include <vector>

class Solution {
  typedef std::pair<int, int> Coordinate;

  void bfs(std::vector<std::vector<int>> &grid, size_t i, size_t j,
           size_t &max_size, std::vector<std::vector<uint8_t>> &explored,
           size_t &current_size) {
    if (i >= grid.size() || j >= grid[0].size() || !grid[i][j] ||
        explored[i][j]) {
      return;
    }

    ++current_size, ++explored[i][j];
    max_size = std::max(current_size, max_size);

    bfs(grid, i + 1, j, max_size, explored, current_size);
    bfs(grid, i - 1, j, max_size, explored, current_size);
    bfs(grid, i, j + 1, max_size, explored, current_size);
    bfs(grid, i, j - 1, max_size, explored, current_size);
  }

public:
  int maxAreaOfIsland(std::vector<std::vector<int>> &grid) {
    std::vector<std::vector<uint8_t>> explored(grid.size());
    for (size_t i = 0; i < grid.size(); ++i) {
      explored[i].resize(grid[0].size());
    }

    size_t max_size(0);
    for (size_t i = 0; i < grid.size(); ++i) {
      for (size_t j = 0; j < grid[0].size(); ++j) {
        if (grid[i][j] == 1) {
          size_t current_size(0);
          bfs(grid, i, j, max_size, explored, current_size);
        }
      }
    }

    return max_size;
  }
};
