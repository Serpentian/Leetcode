/* Search insert position
 *
 *  Given a sorted array of distinct integers and a target value,
 *  return the index if the target is found. If not, return the
 *  index where it would be if it were inserted in order.
 *
 *  You must write an algorithm with O(log n) runtime complexity.
 */

#include <vector>

class Solution {
public:
  int searchInsert(std::vector<int> &nums, int target) {
    int left_bound = 0, right_bound = nums.size() - 1;

    while (left_bound <= right_bound) {
      int mid = left_bound + (right_bound - left_bound) / 2;

      if (nums[mid] > target) {
        right_bound = mid - 1;
      } else if (nums[mid] < target) {
        left_bound = mid + 1;
      } else {
        return mid;
      }
    }

    return left_bound;
  }
};
